import java.util.Scanner;

public class MergeSort {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("Elementlerin sayini daxil edin: ");
        int n = scan.nextInt();
        int arr[] = new int[n];
        System.out.println("Elementlerini daxil edin: ");
        for (int i = 0; i < n; i++) {
            arr[i] = scan.nextInt();
        }

        divide(arr);
        System.out.println("Elementlerin siralanmis formasi: ");
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public static void divide(int[] arr) {
        if (arr.length < 2) {
            return;
        }
        int mid = arr.length / 2;
        int[] left = new int[mid];
        int[] right = new int[arr.length - mid];

        for (int i = 0; i < mid; i++) {
            left[i] = arr[i];
        }
        for (int i = mid; i < arr.length; i++) {
            right[i - mid] = arr[i];
        }
        divide(left);
        divide(right);
        mergeSort(arr, left, right);

    }

    public static void mergeSort(int[] arr, int[] left, int[] right) {

        int i = 0, j = 0, k = 0;
        while (i < left.length && j < right.length) {
            if (left[i] <= right[j]) {
                arr[k++] = left[i++];
            } else {
                arr[k++] = right[j++];
            }
        }
        while (i < left.length) {
            arr[k++] = left[i++];
        }
        while (j < right.length) {
            arr[k++] = right[j++];
        }
    }
}
